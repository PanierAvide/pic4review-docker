# Use the official Node.js image as the base
FROM node:10

# Create folder structure
RUN mkdir -p /app/backend /app/frontend

# Install system dependencies
RUN apt update \
	&& apt install zip \
	&& curl -o- https://raw.githubusercontent.com/transifex/cli/master/install.sh | bash

# Install Pic4Review dependencies
WORKDIR /app/backend
COPY ./Pic4Review-backend/package*.json ./
RUN npm install
WORKDIR /app/frontend
COPY ./Pic4Review/package*.json ./
RUN npm install

# Build backend
COPY ./Pic4Review-backend /app/backend
WORKDIR /app/backend
RUN npm run build

# Build frontend
COPY ./Pic4Review /app/frontend
WORKDIR /app/frontend
RUN npm run build

# Copy the .env file
COPY .env /app

# Expose port on .env
EXPOSE ${PORT}

# Specify the command to run your application
CMD [ "npm", "start" ]
