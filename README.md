# About

Created to help on development of Pic4Review from Adrien Pavie, putting into a single image both Frontend and Backend

Here must be only the code to generate the image, the image itself shall be at DockerHub

# Requeriments

 - docker
 - docker-compose

# How to:

copy the file "dot_env_example", saving it as ".env" and setup the variables there. It will held secrets like the database password, and mappilary api key, thereby it's listed at ".gitignore"

then the "config.json" shall be created using the "write_config.bash":

    bash write_config.bash

check if it's everything allright (any .json linter will do the job) with "Pic4Review-backend/config.json"

after everything is set up, just run docker compose:

    docker-compose up

# Updating

if you update the source code of backend and/or frontend you can then rebuild the docker image using:

    docker-compose up --build

# changing repos 

 you can run
 
    sh cloning_repos.sh
    
to switch to your fork rather than another one (like the original from Adrien)

