#!/bin/bash

source .env # not sure if it stills necessary at the docker creation


# dumping the enviroment variables to the .json
cat > Pic4Review-backend/config.json << EOL
{
	"title": "Pic4Review API configuration file",
	"base_url": "${BASEURL}",
	"db": {
		"host": "${HOST}",
		"user": "${POSTGRES_USER}",
		"pass": "${POSTGRES_PASSWORD}",
		"name": "${POSTGRES_DB}"
	},
	"mail": {
		"from": {
			"address": "${EMAIL_ADDRESS}",
			"server": "${EMAIL_SERVER}",
			"password": "${EMAIL_PASSWORD}",
			"secure": "${EMAIL_PASSWORD}"
		},
		"to": {
			"default": "${EMAIL_PASSWORD}"
		}
	},
	"pictureFetchersCredentials": {
		"mapillary": { "key": "${MAPPILARY_KEY}" }
	},
	"overpass": "${OVERPASS_ADDR}",
	"timers": {
		"firstMissionUpdate": "${F_MISSION_UPDATE}",
		"delayBetweenMissionUpdate": "${DELAY_B_M_UPDATE}"
	},
	"cameraAngle": "${CAMERANGLE}"
}
EOL
